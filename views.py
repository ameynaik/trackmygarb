from flask import render_template, request, redirect, flash,url_for
from flask import Flask, flash, redirect, render_template, request, session, abort
from models import GroceryList, db
from todoapp import app

password = 'mygarbagebin';
username = 'amey';

@app.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        return list_all()
        #return "Hello Boss!  <a href='/logout'>Logout</a>"
 
@app.route('/login', methods=['POST'])
def do_admin_login():
    if request.form['password'] == password and request.form['username'] == username:
        session['logged_in'] = True
        return list_all()
    elif request.form['password'] == 'guest' and request.form['username'] == 'guest':
        return guest_page()
    else:
        return home()
 
@app.route("/guest_page")
def guest_page():
    return render_template(
        	'list.html',
	        grocerylist=GroceryList.query.all(),#join(Priority).order_by(Priority.value.desc())
		)

@app.route("/logout")
def logout():
    session['logged_in'] = False
    return home()

@app.route('/user-homepage')
def list_all():
    if (session['logged_in'] == True):
	    return render_template(
        	'list.html',
	        grocerylist=GroceryList.query.all(),#join(Priority).order_by(Priority.value.desc())
		)
    else:
        return home()

# Listing grocery list by selected category(cat).
@app.route('/<name>')
def list_grocerylist(name): 
    if (session['logged_in'] == True):
    	category = GroceryList.query.filter_by(cat=name).first()
    	return render_template(
    	    'list.html',
    	    grocerylist=GroceryList.query.all(),#join(Priority).order_by(Priority.value.desc())

    	)
    else:
        return home()



@app.route('/<int:glist_id>', methods=['GET', 'POST'])
def update_grocerylist(glist_id):
    if (session['logged_in'] == True):
    	grocerylist = GroceryList.query.get(glist_id)
    	if request.method == 'GET':
    	    return render_template(
    	        'new-category.html',
    	        grocerylist=grocerylist,
    	    )
    	else:
    	    productname = request.form['productname']
    	    grocerylist.productname = productname
    	    db.session.commit()
    	    return redirect('/')
    else:
        return home()

@app.route('/scanned-data', methods=['POST'])
def new_scanned():
    if (request.form['password'] == password and request.form['username'] == username):
        product = GroceryList(productname = request.form['productname'],cat = request.form['cat'],subcat = request.form['subcat'],quantity = request.form['quantity'],price = request.form['price'],logtimestamp = request.form['logtimestamp'])
        db.session.add(product)
        db.session.commit()
        return redirect('/')
    else:
        return home()


@app.route('/new-category', methods=['GET', 'POST'])
def new_category():
    if (session['logged_in'] == True):
    	if (request.method == 'POST'):
	        productname = GroceryList(productname = request.form['productname'])
	        db.session.add(productname)
	        db.session.commit()
	        return redirect('/')
   	else:
        	return render_template(
            	'new-category.html',
	            page='new-category.html')
    else:
        return home()

@app.route('/delete-glist/<int:glist_id>', methods=['POST'])
def delete_glist(glist_id):
    if ((request.method == 'POST') and (session['logged_in'] == True)):
        glist = GroceryList.query.get(glist_id)
        db.session.delete(glist)
        db.session.commit()
        return redirect('/')
    else:
        return home()

#@app.route('/delete-glist/<int:glist_id>', methods=['POST'])
#def delete_glist(glist_id):
#    if request.method == 'POST':
#	print "_____________DEBUG START___________";
#	print glist_id
#	print "_____________DEBUG END___________";
#        glist = GroceryList.query.get(glist_id)
#        db.session.delete(glist)
#        db.session.commit()
#        return redirect('/')

#@app.route('/delete-grocerylist/<int:glist_id>', methods=['POST'])
#def delete_grocerylist(glist_id):
#    if request.method == 'POST':
#	print "_____________DEBUG START___________";
#	print glist_id
#	print "_____________DEBUG END___________";
#        grocerylist = GroceryList.query.get(glist_id)
#        db.session.delete(grocerylist)
#        db.session.commit()
#        return redirect('/')


@app.route('/mark-done/<int:glist_id>', methods=['POST'])
def mark_done(glist_id):
    if ((request.method == 'POST') & (session['logged_in'] == True)):
        grocerylist = GroceryList.query.get(glist_id)
        if (grocerylist.is_purchased == True):
		grocerylist.is_purchased = False
        else:
		grocerylist.is_purchased = True
        db.session.commit()
        return redirect('/')
    else:
        return home()
