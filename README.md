# Trackmygarb

trackmygarb is hosted on heroku. 
 https://trackmygarb.herokuapp.com
 
 When any household item is consumed and its package is about to be thrown in the garbage bin, you scan its barcode using the scanner installed above the garbage bin. 
 
 When a barcode is scanned with a scanner connected to a raspberry pi module, the item gets listed on https://trackmygarb.herokuapp.com.
 Program written here identifies the item's name and its details from the barcode using an opensource UPC (Universal Product Code) database available online.
 This way you have a grocery list ready!
# Screenshots

## Login Page
![](screenshots/trackmygarb_login_page.png)

## Home page with item listed
![](screenshots/trackmygarb_itemlist.png)

## Strike out the item from the list using Green button after purchasing the item
![](screenshots/trackmygarb_strikeout_after_purchase.png)

## Page to add an item manually
![](screenshots/trackmygarb_add_item_manually.png)

