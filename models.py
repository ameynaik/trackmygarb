from datetime import datetime
from flask.ext.sqlalchemy import SQLAlchemy
from todoapp import app

db = SQLAlchemy(app)


# AN added
#	CREATE TABLE GROCERY_LIST(
#	ID SERIAL PRIMARY KEY NOT NULL,
#	PRODUCTNAME TEXT,
#	CAT TEXT,
#	SUBCAT TEXT,
#	QUANTITY TEXT,
#	PRICE REAL,
#	LOGTIMESTAMP TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
#	PURCHASED INT);
class GroceryList(db.Model):
    __tablename__ = "grocerylist"
    id = db.Column('id', db.Integer, primary_key=True)
    productname = db.Column('productname', db.Unicode)
    cat = db.Column('cat', db.Unicode)
    subcat = db.Column('subcat', db.Unicode)
    quantity = db.Column('quantity', db.Unicode)
    price = db.Column('price', db.REAL)
    #logtimestamp = db.Column('logtimestamp', db.Date, default=datetime.utcnow)
    logtimestamp = db.Column('logtimestamp', db.DateTime(timezone=True), default=datetime.utcnow)
    is_purchased = db.Column('is_purchased', db.Boolean, default=False)
