#!/usr/bin/python -tt
# This Python file uses the following encoding: utf-8


# Creator : Amey Naik
# Date : March 26, 2017
# Function Name: ParseWebPageFn
# input - barcode number scanned from barcode scanner (or entered manually)
# output - TimeScanned, ProductName, Catergory, SubCategory, Size, Price

import sys,os,re
from datetime import datetime #FIXME: Delete later
import psycopg2
import urlparse
import time
from ParseBuycott import ParseBuycott
from Parseupcdatabase_org import Parseupcdatabase_org
from Parseupcdatabase import Parseupcdatabase
from Parseupcitemdb import Parseupcitemdb

def main():
  #if len(sys.argv) != 2:
  #  print 'usage: ./ParseWebPageFn.py --BarcodeNumber'
  #  sys.exit(1)
  
  #BarcodeNumber = sys.argv[1] 
  while True:
	#Reset Everything
	Price = '0'
	Quantity = '0'

  	BarcodeNumber = raw_input('\n \n Please scan a Barcode ')
  	print BarcodeNumber
  	
	if (BarcodeNumber == '000'): #FIXME
		sys.exit(1)
  	# Append BarcodeNumber to the website Name
  	# Website Name : https://www.barcodespider.com/
  	WebsiteName = 'https://www.barcodespider.com/';
  	ProductUrl = WebsiteName+BarcodeNumber+'\n';
  	print ProductUrl

  	f = open('shellscripttemp.sh', 'w')
  	f.write('curl '+ProductUrl)
  	f.close()

  	command = 'bash shellscripttemp.sh > temp'
  	print command
  	os.system(command);

	# Check if Product is found on database
	if (os.stat("temp").st_size > 0):

  		str2matchName = 'UPC code number '+BarcodeNumber+' is associated with <h3>';
  		str2matchCat = '<td>Category:';

  		f = open('temp', 'r') 
  		for line in f: 
        	      m = re.search('\$[0-9]+.[0-9]+',line)
  		      if str2matchName in line:
  		      	LineWithNameQuant = line
		      elif str2matchCat in line:
			LineWithCat = (next(f, '').strip())

		      if m:
			Pricewithdollar = m.group(0)
			Price = Pricewithdollar[1:]

		# Name and Quantity parsing
  		LineWithNameQuantP1 = re.search('<h3>.*', LineWithNameQuant)
  		LineWithNameQuantP2 = LineWithNameQuantP1.group(0)
  		LineWithNameQuantP3 = LineWithNameQuantP2[4:-5]
  		
  		LineWithNameQuantP3 = LineWithNameQuantP3.split(",")
		if len(LineWithNameQuantP3) > 1:
			#print '\nProduct Name : '+LineWithNameQuantP3[0]+'\n'+'Quantity : '+LineWithNameQuantP3[1]+'\n'
			ProductName = LineWithNameQuantP3[0];
			Quantity = LineWithNameQuantP3[1];
		else:
			#print '\nProduct Name : '+LineWithNameQuantP3[0]+'\n'+'Quantity : '+'1'+'\n'
			ProductName = LineWithNameQuantP3[0];
			Quantity = '1';

		# Category parsing
  		LineWithCatP1 = re.search('<td>.*', LineWithCat)
  		LineWithCatP2 = LineWithCatP1.group(0)
  		LineWithCatP3 = LineWithCatP2[4:-5]
		
		LineWithCatP3 = LineWithCatP3.replace("&raquo;",'>')
  		LineWithCatP3 = LineWithCatP3.split(">")
		#print 'Category : '+LineWithCatP3[0]+'\n'
		#print 'Subcategory : '+LineWithCatP3[1]+'\n'
		Category = LineWithCatP3[0];
		Subcategory = LineWithCatP3[1];
		TimeScanned = time.strftime("%c"); #str(datetime.now());

		#print 'TimeScanned,ProductName,Quantity,Category,Subcategory,Price\n'
		Details = [TimeScanned,ProductName,Quantity,Category,Subcategory,Price]
		print ProductName+'\n'+Quantity+'\n'+Category+'\n'+Subcategory+'\n'+Price+'\n'+TimeScanned+'\n'

		# to avoid % in the POST string
		ProductName=ProductName.replace("%",'')
		Category=Category.replace("%",'')
		Subcategory=Subcategory.replace("%",'')
		Quantity=Quantity.replace("%",'')
		Price=Price.replace("%",'')
		f = open('shellscript_scannedproduct.sh', 'w')
		#f.write('curl '+ProductUrl)
		f.write('curl -X POST -F \'productname='+ProductName+'\' -F \'cat='+Category+'\' -F \'subcat='+Subcategory+'\' -F \'quantity='+Quantity+'\' -F \'logtimestamp='+TimeScanned+'\' -F \'price='+Price+'\' -F \'username=amey\' -F \'password=mygarbagebin\' http://trackmygarb.herokuapp.com/scanned-data');
		f.close()

		command_scanned = 'bash shellscript_scannedproduct.sh'
  		print command_scanned+"first"
  		os.system(command_scanned);

		
		#Reset Everything #FIXME
		Price = '0'
		Quantity = '0'

	else: # try upcdatabase.org
		(ProductName, PatternFound,Price,Category) = Parseupcdatabase_org(BarcodeNumber)
		if PatternFound:
			Subcategory = 'NA';
			TimeScanned = time.strftime("%c");

# to avoid % in the POST string
			ProductName = ProductName.replace("%",'')
			Category    = Category.replace("%",'')
			Subcategory = Subcategory.replace("%",'')
			Quantity    = Quantity.replace("%",'')
			Price       = Price.replace("%",'')

			f = open('shellscript_scannedproduct.sh', 'w')
			f.write('curl -X POST -F \'productname='+ProductName+'\' -F \'cat='+Category+'\' -F \'subcat='+Subcategory+'\' -F \'quantity='+Quantity+'\' -F \'logtimestamp='+TimeScanned+'\' -F \'price='+Price+'\' -F \'username=amey\' -F \'password=mygarbagebin\' http://trackmygarb.herokuapp.com/scanned-data');
			f.close()

			command_scanned = 'bash shellscript_scannedproduct.sh'
  			print command_scanned+"upcdatabase.org"
  			os.system(command_scanned);

		else: # try buycott.com 
			(ProductName, PatternFound) = ParseBuycott(BarcodeNumber) # buycott.com is useful only to know the product name. No other info is available
			if PatternFound:
				Category = 'NA';
				Subcategory = 'NA';
				Quantity = 'NA';
				TimeScanned = time.strftime("%c");
				Price = '0';

# to avoid % in the POST string
				ProductName = ProductName.replace("%",'')
				Category    = Category.replace("%",'')
				Subcategory = Subcategory.replace("%",'')
				Quantity    = Quantity.replace("%",'')
				Price       = Price.replace("%",'')
				f = open('shellscript_scannedproduct.sh', 'w')
				f.write('curl -X POST -F \'productname='+ProductName+'\' -F \'cat='+Category+'\' -F \'subcat='+Subcategory+'\' -F \'quantity='+Quantity+'\' -F \'logtimestamp='+TimeScanned+'\' -F \'price='+Price+'\' -F \'username=amey\' -F \'password=mygarbagebin\' http://trackmygarb.herokuapp.com/scanned-data');
				f.close()

				command_scanned = 'bash shellscript_scannedproduct.sh'
  				print command_scanned+"buycott.com"
  				os.system(command_scanned);
			else:
				(ProductName,Price,PatternFound) = Parseupcitemdb(BarcodeNumber) #
				if PatternFound:
					Category = 'NA';
					Subcategory = 'NA';
					Quantity = 'NA';
					Price = str(Price)
					TimeScanned = time.strftime("%c");
# to avoid % in the POST string
					ProductName  = ProductName.replace("%",'')
					Category     = Category.replace("%",'')
					Subcategory  = Subcategory.replace("%",'')
					Quantity     = Quantity.replace("%",'')
					Price	     = Price.replace("%",'')

					f = open('shellscript_scannedproduct.sh', 'w')
					f.write('curl -X POST -F \'productname='+ProductName+'\' -F \'cat='+Category+'\' -F \'subcat='+Subcategory+'\' -F \'quantity='+Quantity+'\' -F \'logtimestamp='+TimeScanned+'\' -F \'price='+Price+'\' -F \'username=amey\' -F \'password=mygarbagebin\' http://trackmygarb.herokuapp.com/scanned-data');
					f.close()

					command_scanned = 'bash shellscript_scannedproduct.sh'
  					print command_scanned+"Parseupcitemdb"
  					os.system(command_scanned);
				
				else:
					(ProductName,Quantity) = Parseupcdatabase(BarcodeNumber) #
					print ProductName
					print Quantity
					Category = 'NA';
					Subcategory = 'NA';
					TimeScanned = time.strftime("%c");
					Price = '0';

# to avoid % in the POST string
					ProductName = ProductName.replace("%",'')
					Category    = Category.replace("%",'')
					Subcategory = Subcategory.replace("%",'')
					Quantity    = Quantity.replace("%",'')
					Price       = Price.replace("%",'')

					f = open('shellscript_scannedproduct.sh', 'w')
					f.write('curl -X POST -F \'productname='+ProductName+'\' -F \'cat='+Category+'\' -F \'subcat='+Subcategory+'\' -F \'quantity='+Quantity+'\' -F \'logtimestamp='+TimeScanned+'\' -F \'price='+Price+'\' -F \'username=amey\' -F \'password=mygarbagebin\' http://trackmygarb.herokuapp.com/scanned-data');
					f.close()

					command_scanned = 'bash shellscript_scannedproduct.sh'
  					print command_scanned+"Parseupcdatabase"
  					os.system(command_scanned);

if __name__ == '__main__':
  main()
  #print ("%s" % (time.time() - start_time))
