#!/usr/bin/python -tt
# This Python file uses the following encoding: utf-8


# Creator : Amey Naik
# Date : March 26, 2017
# Function Name: Parseupcitemdb
# input - barcode number scanned from barcode scanner (or entered manually)

import sys,os,re
from datetime import datetime #FIXME: Delete later
import psycopg2
import urlparse
import time
import urllib
import json

def Parseupcitemdb(BarcodeNumber):
	print "In parse upcdatabase" + BarcodeNumber
  	WebsiteName = ' https://api.upcitemdb.com/prod/trial/lookup?upc=';
  	ProductUrl = WebsiteName+BarcodeNumber+'\n';
	print ProductUrl

	j = urllib.urlopen(ProductUrl)
	j_obj = json.load(j)
		
	code = j_obj['code']
	PatternFound = 0	

	if (code == 'OK') and len(j_obj['items'])!=0:
		PatternFound = 1	
		ProductName = j_obj['items'][0]['title']
		#Price = j_obj['items'][0]['offers'][0]['price']
		Price = 0
		print 	ProductName, Price
		return (ProductName,Price,PatternFound)
	
	else:
		ProductName = BarcodeNumber	
		Price = 0
		return (ProductName,Price,PatternFound)
