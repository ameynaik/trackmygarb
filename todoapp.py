import os
from flask import Flask

app = Flask(__name__)
#app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgres@localhost/todoapp'
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL'] #'postgresql://postgres:postgres@localhost/todoapp'
app.secret_key = '' #Add secret key here!

from views import *


if __name__ == '__main__':
    app.run()



