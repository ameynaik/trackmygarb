#!/usr/bin/python -tt
# This Python file uses the following encoding: utf-8


# Creator : Amey Naik
# Date : March 26, 2017
# Function Name: Parseupcdatabase
# input - barcode number scanned from barcode scanner (or entered manually)

import sys,os,re
from datetime import datetime #FIXME: Delete later
import psycopg2
import urlparse
import time


def Parseupcdatabase(BarcodeNumber):
	print "In parse upcdatabase" + BarcodeNumber
  	WebsiteName = 'https://www.upcdatabase.com/item/';
  	ProductUrl = WebsiteName+BarcodeNumber+'\n';
	print ProductUrl


  	f = open('shellupcdatabase.sh', 'w')
  	f.write('curl '+ProductUrl)
  	f.close()

  	command = 'bash shellupcdatabase.sh > tempupcdatabase'
  	print command
  	os.system(command);

	PatternFound = 0	
	str2matchName = 'Description';
	f = open('tempupcdatabase', 'r') 
	for line in f:
		if str2matchName in line:
  		      	LineWithName = line
			PatternFound = 1
			break

	if PatternFound:
		print LineWithName 
		
		LineWithNameSplit = LineWithName.split("<td>");
		LineWithNameSplitDesp =  LineWithNameSplit[3]
		LineWithNameSplit = LineWithNameSplitDesp.split("</td></tr>\n");
		ProductName = LineWithNameSplit[0]


		QuantFound = 0
		str2matchQuant = "Size/Weight"
		f = open('tempupcdatabase', 'r') 
		for line in f:
			if str2matchQuant in line:
  			      	LineWithName = line
				QuantFound = 1
				break


		if QuantFound:
			print LineWithName 
			
			LineWithNameSplit = LineWithName.split("<td>");
			LineWithNameSplitDesp =  LineWithNameSplit[3]
			LineWithNameSplit = LineWithNameSplitDesp.split("</td></tr>\n");
			Quantity = LineWithNameSplit[0]
		else:
			Quantity = 'NA'

		return (ProductName,Quantity)
	else:
		return (BarcodeNumber,'NA')
	
	
