#!/usr/bin/python -tt
# This Python file uses the following encoding: utf-8


# Creator : Amey Naik
# Date : Feb 3, 2018
# Function Name: Parseupcdatabase_org (from www.upcdatabase.org)
# input - barcode number scanned from barcode scanner (or entered manually)

import sys,os,re
from datetime import datetime #FIXME: Delete later
import psycopg2
import urlparse
import time


def Parseupcdatabase_org(BarcodeNumber):
	print "In parse Parseupcdatabase_org" + BarcodeNumber
  	WebsiteName = 'https://upcdatabase.org/code/';
  	ProductUrl = WebsiteName+BarcodeNumber+'\n';
	print ProductUrl


  	f = open('shellscripttempupcdatabase_org.sh', 'w')
  	f.write('curl '+ProductUrl)
  	f.close()

  	command = 'bash shellscripttempupcdatabase_org.sh > tempupcdatabase_org'
  	print command
  	os.system(command);

	f = open('tempupcdatabase_org', 'r') 

	str2match = "Information on barcode"
	PatternFound = 0
	for line in f:
		if str2match in line:
			PatternFound = 1

	print 'PATTERN IS FOUND  :   '+str(PatternFound)

	if PatternFound:
		
		str2matchName = 'th colspan=\"';
		f = open('tempupcdatabase_org', 'r') 
		for line in f:
			if str2matchName in line:
  			      	LineWithName = line
				break

		#print 'CHECKER' + LineWithName 
		LineWithNameSplit = LineWithName.split(">");
		#print 'CHECKER' + LineWithNameSplit[2][0:-4]
		ProductName = LineWithNameSplit[2][0:-4]

		str2matchPrice = 'MSRP';

		for line in f:
			if str2matchPrice in line:
  			      	LineWithPrice = next(f)
				break
		LineWithPriceSplit = LineWithPrice.split(">");
		#print LineWithPriceSplit[1]
		Price = LineWithPriceSplit[1][1:-4] # cannot have '$' 
		#print 'CHECK price ' + Price


		str2matchCat = 'Category';

		for line in f:
			if str2matchCat in line:
  			      	LineWithCat = next(f)
				break
		LineWithCatSplit = LineWithCat.split(">");
		#print LineWithCatSplit[1]
		Category = LineWithCatSplit[1][0:-4]
		#print 'CHECK cat ' + Category

		print ProductName + ' ' + Price + ' ' + Category

		return (ProductName,PatternFound,Price,Category)
	else:
		return ('NA',PatternFound,0,'NA')
	
